#![no_std]

use core::fmt;
use core::{cell::RefCell, ops::DerefMut};
use cortex_m::interrupt::{self, CriticalSection, Mutex};
use imxrt_usbd::full_speed::BusAdapter;
use static_cell::StaticCell;
use usb_device::{class_prelude::*, prelude::*};
use usbd_serial::SerialPort;

pub type Filter = (&'static str, Option<::log::LevelFilter>);

/// Filters for enabling logs
pub struct Filters(pub &'static [Filter]);

impl Filters {
    /// Returns `true` if, based on this metadata, logging should be enabled
    ///
    /// `is_enabled()` considers the permitted modules and log levels for those modules.
    pub fn is_enabled(&self, metadata: &::log::Metadata) -> bool {
        if self.0.is_empty() {
            true
        } else if let Some(idx) = self
            .0
            .iter()
            .position(|&(target, _)| target == metadata.target())
        {
            let (_, lvl) = self.0[idx];
            lvl.is_none() || lvl.filter(|lvl| metadata.level() <= *lvl).is_some()
        } else {
            false
        }
    }
}

pub struct LoggingConfig {
    /// The max log level for *all* logging
    ///
    /// By default, we select the static max level. Users may
    /// override this if they'd like to bypass the statically-assigned
    /// max level
    pub max_level: ::log::LevelFilter,
    /// A list of filtered targets to log.
    ///
    /// If set to an empty slice (default), the logger performs no
    /// filtering. Otherwise, we filter the specified targets by
    /// the accompanying log level. See [`Filter`](type.Filter.html) for
    /// more information.
    pub filters: &'static [Filter],
}

impl Default for LoggingConfig {
    fn default() -> LoggingConfig {
        LoggingConfig {
            max_level: ::log::STATIC_MAX_LEVEL,
            filters: &[],
        }
    }
}

/// An error that indicates the logger is already set
///
/// The error could propagate from one of the `init()` functions.
/// Or, it could propagate if the underlying logger was set through another logging
/// interface.
#[derive(Debug)]
pub struct SetLoggerError(());

impl From<::log::SetLoggerError> for SetLoggerError {
    fn from(_: ::log::SetLoggerError) -> SetLoggerError {
        SetLoggerError(())
    }
}

pub struct Sink {
    serial: SerialPort<'static, BusAdapter>,
    device: UsbDevice<'static, BusAdapter>,
}

impl fmt::Write for Sink {
    fn write_str(&mut self, string: &str) -> fmt::Result {
        self.serial
            .write(string.as_bytes())
            .map_err(|_| fmt::Error)?;
        Ok(())
    }
}

struct Logger {
    /// The peripheral
    usb: Mutex<RefCell<Sink>>,
    /// A collection of targets that we are expected
    /// to filter. If this is empty, we allow everything
    filters: Filters,
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &::log::Metadata) -> bool {
        metadata.level() <= ::log::max_level() // The log level is appropriate
                    && self.filters.is_enabled(metadata) // The target is in the filter list
    }

    fn log(&self, record: &::log::Record) {
        if self.enabled(record.metadata()) {
            interrupt::free(|cs| {
                let usb = self.usb.borrow(cs);
                let mut usb = usb.borrow_mut();
                use core::fmt::Write;
                write!(
                    usb,
                    "[{} {}]: {}\r\n",
                    record.level(),
                    record.target(),
                    record.args()
                )
                .expect("write should not fail");
            });
        }
    }

    fn flush(&self) {
        interrupt::free(|cs| {
            let usb = self.usb.borrow(cs);
            let mut usb = usb.borrow_mut();
            usb.serial.flush().ok();
        })
    }
}

static BUS: StaticCell<UsbBusAllocator<BusAdapter>> = StaticCell::new();
static LOGGER: StaticCell<Logger> = StaticCell::new();

pub struct Poller {
    logger: &'static Logger,
}

impl Poller {
    pub fn poll(&self, cs: &CriticalSection) {
        static mut IS_CONFIGURED: bool = false;

        let logger = self.logger.usb.borrow(cs);
        let mut logger = logger.borrow_mut();
        let logger = logger.deref_mut();

        let device = &mut logger.device;
        if device.poll(&mut [&mut logger.serial]) {
            // Safety: as long as this function is only called from a single
            // USB_OTG interrupt, we are free to read/write `IS_CONFIGURED`.
            if device.state() == usb_device::device::UsbDeviceState::Configured {
                if unsafe { !IS_CONFIGURED } {
                    device.bus().configure();
                }
                unsafe {
                    IS_CONFIGURED = true;
                }
            } else {
                unsafe {
                    IS_CONFIGURED = false;
                }
            }
        }
    }
}

pub fn init(
    bus: UsbBusAllocator<BusAdapter>,
    config: LoggingConfig,
) -> Result<Poller, SetLoggerError> {
    let bus: &'static mut UsbBusAllocator<BusAdapter> = BUS.init(bus);

    let serial = usbd_serial::SerialPort::new(bus);
    let device = UsbDeviceBuilder::new(bus, UsbVidPid(0x5824, 0x27dd))
        .product("imxrt-usbd")
        .device_class(usbd_serial::USB_CLASS_CDC)
        .build();

    let logger = LOGGER.init_with(|| Logger {
        usb: Mutex::new(RefCell::new(Sink { serial, device })),
        filters: Filters(config.filters),
    });

    ::log::set_logger(logger).map(|_| ::log::set_max_level(config.max_level))?;

    Ok(Poller { logger })
}
