//! Teensy 4 example for usb logging with polling from interrupt

#![no_std]
#![no_main]

use teensy4_bsp as bsp;
use teensy4_panic as _;

use bsp::interrupt;
use core::cell::RefCell;
use core::time::Duration;
use cortex_m::interrupt::Mutex;

use imxrt_usb_log;

mod support;

const LED_PERIOD: Duration = Duration::from_millis(1_000);
/// The GPT output compare register we're using for
/// tracking time. This is the first register, since
/// we're using reset mode.
const GPT_OCR: bsp::hal::gpt::OutputCompareRegister = bsp::hal::gpt::OutputCompareRegister::One;

#[cortex_m_rt::entry]
fn main() -> ! {
    let mut periphs = bsp::Peripherals::take().unwrap();

    // Reduce the number of pins to those specific to the Teensy 4.0.
    let pins = bsp::pins::t40::from_pads(periphs.iomuxc);
    // Prepare the LED, and turn it on!
    let mut led = bsp::configure_led(pins.p13);
    led.set();

    // Prepare the ARM clock to run at ARM_HZ.
    periphs.ccm.pll1.set_arm_clock(
        bsp::hal::ccm::PLL1::ARM_HZ,
        &mut periphs.ccm.handle,
        &mut periphs.dcdc,
    );

    // Prepare a GPT timer for blocking delays.
    let mut timer = {
        // Run PERCLK on the crystal oscillator (24MHz).
        let mut cfg = periphs.ccm.perclk.configure(
            &mut periphs.ccm.handle,
            bsp::hal::ccm::perclk::PODF::DIVIDE_1,
            bsp::hal::ccm::perclk::CLKSEL::OSC,
        );

        let mut gpt1 = periphs.gpt1.clock(&mut cfg);
        // Keep ticking if we enter wait mode.
        gpt1.set_wait_mode_enable(true);
        // When the first output compare register compares,
        // reset the counter back to zero.
        gpt1.set_mode(bsp::hal::gpt::Mode::Reset);
        // Compare every LED_PERIOD ticks.
        gpt1.set_output_compare_duration(GPT_OCR, LED_PERIOD);
        gpt1
    };
    timer.set_enable(true);

    let (ccm, ccm_analog) = periphs.ccm.handle.raw();
    support::ccm::initialize(ccm, ccm_analog);

    let bus_adapter = support::new_bus_adapter();
    bus_adapter.set_interrupts(true);
    let bus = usb_device::bus::UsbBusAllocator::new(bus_adapter);

    let poller = imxrt_usb_log::init(bus, Default::default()).unwrap();
    setup(poller);
    led.set();

    loop {
        if timer.output_compare_status(GPT_OCR).is_set() {
            led.toggle();
            timer.output_compare_status(GPT_OCR).clear();

            log::info!("Hello World");
        }
    }
}

/// Setup the USB ISR with the USB poller
fn setup(poller: imxrt_usb_log::Poller) {
    static POLLER: Mutex<RefCell<Option<imxrt_usb_log::Poller>>> = Mutex::new(RefCell::new(None));

    #[cortex_m_rt::interrupt]
    fn USB_OTG1() {
        cortex_m::interrupt::free(|cs| {
            POLLER
                .borrow(cs)
                .borrow_mut()
                .as_mut()
                .map(|poller| poller.poll(cs));
        });
    }

    cortex_m::interrupt::free(|cs| {
        *POLLER.borrow(cs).borrow_mut() = Some(poller);
        // Safety: invoked in a critical section that also prepares the ISR
        // shared memory. ISR memory is ready by the time the ISR runs.
        unsafe { cortex_m::peripheral::NVIC::unmask(bsp::interrupt::USB_OTG1) };
    });
}
