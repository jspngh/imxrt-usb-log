# Rust library for logging over USB with imxrt-hal
This crate is now deprecated.
Use the new logging functionality included in [imxrt-hal](https://crates.io/crates/imxrt-log).

## Why this crate
Originally, imxrt-hal did have support USB based logging,
but only through a C library.

This library was an attempt to achieve the same, but purely using Rust.
